use std::{
    cell::RefCell,
    env,
    io::{self, Write},
    process::ExitCode,
    rc::Rc,
};

use cmd::{errors::CmdError, models::{CmdFn, Command, CommandBuilder, CommandsBuilder}};


fn main() -> ExitCode {
    let stdout: io::Stdout = io::stdout();
    let stdout_rc = Rc::new(RefCell::new(stdout));
    let args: Vec<String> = env::args().collect();
    let program_name = args[0].clone();

    build_commands(args, stdout_rc, program_name)
}

fn build_commands(args: Vec<String>, stdout_rc: Rc<RefCell<io::Stdout>>, program_name: String) -> ExitCode {
    let mut commands = CommandsBuilder::new()
        .padding(4)
        .addCommand(
            CommandBuilder::new()
                .name("case")
                .description("a command to change the case of a text which accepts sub commands")
                .sub_commands(
                    CommandsBuilder::new()
                        .padding(4)
                        .addCommand(
                            CommandBuilder::new()
                                .name("upper")
                                .description("lowers the case of the arguments")
                                .func(get_to_upper())
                                .build()
                                .unwrap(),
                        )
                        .addCommand(
                            CommandBuilder::new()
                                .name("lower")
                                .description("uppers the case of the arguments")
                                .func(get_to_lower())
                                .build()
                                .unwrap(),
                        )
                        .build()
                        .unwrap(),
                )
                .build()
                .unwrap(),
        )
        .addCommand(CommandBuilder::new()
            .name("Help")
            .description("a command to display help")
            .build()
            .unwrap()
        )
        .build()
        .unwrap();

    let result = commands.run(&args, stdout_rc.clone(), program_name.clone());

    stdout_rc.borrow_mut().write_all(b"\n").unwrap();
    stdout_rc.borrow_mut().flush().unwrap();

    if let Err(e) = result {
        eprintln!("ERROR: {}", e);
        commands.help(stdout_rc, None, &program_name).unwrap();
        return ExitCode::FAILURE;
    }

    ExitCode::SUCCESS
}

fn get_to_upper() -> CmdFn {
    Box::new(
        |writer: Rc<RefCell<dyn Write>>, args: &[String]| -> Result<(), CmdError> {
            let result: String = args
                .iter()
                .map(|arg| arg.to_uppercase())
                .collect::<Vec<String>>()
                .join(" ");
            let mut writer = writer.borrow_mut();

            writer.write_all(result.as_bytes())?;
            writer.flush()?;

            Ok(())
        },
    )
}

fn get_to_lower() -> CmdFn {
    Box::new(
        |writer: Rc<RefCell<dyn Write>>, args: &[String]| -> Result<(), CmdError> {
            let result: String = args
                .iter()
                .map(|arg| arg.to_lowercase())
                .collect::<Vec<String>>()
                .join(" ");

            let mut writer = writer.borrow_mut();

            writer.write_all(result.as_bytes())?;
            writer.flush()?;
            Ok(())
        },
    )
}
