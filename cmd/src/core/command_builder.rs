use std::{cell::RefCell, io::Write, mem::take, rc::Rc};

use crate::{
    errors::{CmdError, CommandBuilderError, CommandsBuilderError},
    models::{Command, CommandBuilder, Commands, CommandsBuilder, Name, NoName},
};

impl<'a, F> CommandBuilder<'a, F, NoName>{

    pub fn new() -> Self {
        CommandBuilder {
            name: NoName,
            description: None,
            func: None,
            sub_commands: None,
        }
    }
}


impl<'a, F> CommandBuilder<'a, F, Name>{

    pub fn build(&mut self) -> Result<Command<'a, F>, CommandBuilderError> {
        let description = self
            .description
            .ok_or_else(|| CommandBuilderError::NameNotFound)?;

        Ok(Command {
            name: Name(self.name.to_string()),
            description,
            func: take(&mut self.func),
            sub_commands: take(&mut self.sub_commands),
        })
    }
}


impl<'a, F, N> CommandBuilder<'a, F, N>
where
    F: FnMut(Rc<RefCell<dyn Write>>, &[String]) -> Result<(), CmdError>,
{


    pub fn name(&mut self, name: impl Into<String>) -> CommandBuilder<'a, F, Name> {
        CommandBuilder{
            name: Name(name.into()),
            description: self.description,
            func: self.func.take(),
            sub_commands: self.sub_commands.take()
        }
    }

    pub fn description(&mut self, description: &'a str) -> &mut Self {
        self.description = Some(description);
        self
    }

    pub fn func(&mut self, func: F) -> &mut Self {
        self.func = Some(func);
        self
    }

    pub fn sub_commands(&mut self, sub_commands: Commands<'a, F>) -> &mut Self {
        self.sub_commands = Some(Box::new(sub_commands));
        self
    }
}

impl<'a, F> CommandsBuilder<'a, F>
where
    F: FnMut(Rc<RefCell<dyn Write>>, &[String]) -> Result<(), CmdError>,
{
    pub fn new() -> CommandsBuilder<'a, F> {
        CommandsBuilder {
            command_list: None::<Vec<Command<'a, F>>>,
            padding: None,
        }
    }

    pub fn padding(&mut self, padding: usize) -> &mut CommandsBuilder<'a, F> {
        self.padding = Some(padding);
        self
    }

    pub fn addCommand(&mut self, command: Command<'a, F>) -> &mut CommandsBuilder<'a, F> {
        if self.command_list.is_none() {
            self.command_list = Some(Vec::new());
        }

        self.command_list.as_mut().map(|x| x.push(command));
        self
    }

    pub fn build(&mut self) -> Result<Commands<'a, F>, CommandsBuilderError> {
        let padding = self.padding.unwrap_or(4);
        let command_list = self
            .command_list
            .take()
            .ok_or(CommandsBuilderError::CommandsNotFound)?;

        Ok(Commands {
            padding,
            command_list,
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        errors::{CmdError, CommandBuilderError},
        models::{CmdFn, CommandBuilder, CommandsBuilder},
    };
    use std::{cell::RefCell, io::Write, rc::Rc};

    fn get_to_x() -> CmdFn {
        Box::new(
            |writer: Rc<RefCell<dyn Write>>, args: &[String]| -> Result<(), CmdError> {
                let result: Vec<String> = args
                    .into_iter()
                    .map(|str| str.chars().into_iter().map(|_s| "X".to_owned()).collect())
                    .collect();
                let mut writer = writer.borrow_mut();

                result
                    .iter()
                    .try_for_each(|item: &String| -> Result<(), CmdError> {
                        writeln!(writer, "{}", item)?;
                        Ok(())
                    })?;
                Ok(())
            },
        )
    }

    #[test]
    fn should_build_simple_command() {
        let command = CommandBuilder::new()
            .name("simple-command")
            .description("a simple command built with a builder")
            .func(get_to_x())
            .build();

        let writer: Rc<RefCell<Vec<u8>>> = Rc::new(RefCell::new(Vec::new()));

        let args: Vec<String> = ["QSf", "AZERAZER"].iter().map(|s| s.to_string()).collect();

        let result = command.unwrap().run(writer.clone(), &args);

        assert!(result.is_ok());
        assert_eq!(
            String::from_utf8(writer.clone().borrow().to_vec()).unwrap(),
            "XXX\nXXXXXXXX\n"
        );
    }

    #[test]
    fn should_help_for_simple_command() {
        let command = CommandBuilder::new()
            .name("simple-command")
            .description("a simple command built with a builder")
            .func(get_to_x())
            .build();

        let writer: Rc<RefCell<Vec<u8>>> = Rc::new(RefCell::new(Vec::new()));

        let result = command.unwrap().help(writer.clone(), 6);

        assert!(result.is_ok());
        assert_eq!(
            String::from_utf8(writer.clone().borrow().to_vec()).unwrap(),
            "\n        simple-command   - a simple command built with a builder"
        );
    }

    #[test]
    fn should_build_sub_command_command() {
        let command = CommandBuilder::new()
            .name("simple-subcommand")
            .description("a simple subcommand built with a builder")
            .sub_commands(
                CommandsBuilder::new()
                    .padding(6)
                    .addCommand(
                        CommandBuilder::new()
                            .name("sub-command")
                            .description("a subcommand")
                            .func(get_to_x())
                            .build()
                            .unwrap(),
                    )
                    .build()
                    .unwrap(),
            )
            .build();

        let writer: Rc<RefCell<Vec<u8>>> = Rc::new(RefCell::new(Vec::new()));

        let args: Vec<String> = ["sub-command", "AZERAZER", "ty"]
            .iter()
            .map(|s| s.to_string())
            .collect();

        let result = command.unwrap().run(writer.clone(), &args);

        assert!(result.is_ok());
        assert_eq!(
            String::from_utf8(writer.clone().borrow().to_vec()).unwrap(),
            "XXXXXXXX\nXX\n"
        );
    }

    #[test]
    fn should_help_sub_command_command() {
        let command = CommandBuilder::new()
            .name("command")
            .description("a simple command ith one subcommand built with a builder")
            .sub_commands(
                CommandsBuilder::new()
                    .padding(6)
                    .addCommand(
                        CommandBuilder::new()
                            .name("sub-command")
                            .description("a subcommand built with a builder")
                            .func(get_to_x())
                            .build()
                            .unwrap(),
                    )
                    .build()
                    .unwrap(),
            )
            .build();

        let writer: Rc<RefCell<Vec<u8>>> = Rc::new(RefCell::new(Vec::new()));

        let result = command.unwrap().help(writer.clone(), 6);

        assert!(result.is_ok());
        assert_eq!(
            String::from_utf8(writer.clone().borrow().to_vec()).unwrap(),
            "\n        command   - a simple command ith one subcommand built with a builder\n\n      Usage:   command [Sub command]\n      example: command help\n\n      Sub command list:\n\n        sub-command   - a subcommand built with a builder\n"
        );
    }

    #[test]
    fn test_command_builder_without_description() {
        let command = CommandBuilder::new()
            .name("test")
            .func(|_, _| Ok(()))
            .build();

        assert!(command.is_err());
        assert_eq!(command.err(), Some(CommandBuilderError::NameNotFound));
    }

    #[test]
    fn test_commands_builder_without_padding() {
        let  command_builder = CommandBuilder::new()
            .name("test")
            .description("test command")
            .func(|_, _| Ok(()))
            .build();

        let command = CommandsBuilder::new().addCommand(command_builder.unwrap()).build().unwrap();

        assert_eq!(command.padding, 4);
    }
}
