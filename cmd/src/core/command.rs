use std::{cell::RefCell, io::Write, rc::Rc};

use crate::{errors::CmdError, models::{Command, Commands}};

impl<'a, F> Command<'a, F> {
    pub fn help(&self, writer: Rc<RefCell<dyn Write>>, padding: usize) -> Result<(), CmdError> {
        let padding = " ".repeat(padding);

        let help = format!("\n{padding}  {}   - {}", self.name, self.description);
        writer.borrow_mut().write_all(help.as_bytes())?;

        if let Some(sub_command) = &self.sub_commands {
            writer.borrow_mut().write_all(b"\n")?;
            sub_command.help(writer.clone(), Some("Sub command"), &self.name.0)?;
        }

        writer.borrow_mut().flush()?;
        Ok(())
    }
}

impl<'a, F> Commands<'a, F> {
    pub fn find_command_by_name(&mut self, name: &str) -> Option<&mut Command<'a, F>> {
        self.command_list
            .iter_mut()
            .find(move |cmd| cmd.name.eq(name))
    }

    pub fn help(
        &self,
        writer: Rc<RefCell<dyn Write>>,
        label: Option<&str>,
        program_name: &str,
    ) -> Result<(), CmdError> {
        let padding = " ".repeat(self.padding);
        writer.borrow_mut().write_all(
            format!(
                "\n{padding}Usage:   {} [{}]\n{padding}example: {} help\n",
                program_name,
                label.unwrap_or("COMMAND"),
                program_name
            )
            .as_bytes(),
        )?;

        writer
            .borrow_mut()
            .write_all(format!("\n{}{} list:\n", padding, label.unwrap_or("COMMAND")).as_bytes())?;

        for cmd in &self.command_list {
            cmd.help(writer.clone(), self.padding)?;
        }

        writer.borrow_mut().write_all(b"\n")?;
        writer.borrow_mut().flush()?;
        Ok(())
    }
}

impl<'a, F> Command<'a, F>
where
    F: FnMut(Rc<RefCell<dyn Write>>, &[String]) -> Result<(), CmdError>,
{
    pub fn run(
        &mut self,
        writer: Rc<RefCell<dyn Write>>,
        sub_args: &[String],
    ) -> Result<(), CmdError> {
        let sub_command_name = sub_args.get(0);

        if let Some(sub_command) = sub_command_name.and_then(|sub_command_name| {
            self.sub_commands
                .as_mut()
                .and_then(|sc| sc.find_command_by_name(sub_command_name))
        }) {

            (sub_command
                .func
                .take()
                .ok_or_else(|| CmdError::CommandNotFound)?)(writer.clone(), sub_args.get(1..).ok_or_else(|| CmdError::MissingArguments)?)

        } else {
            (self.func.take().ok_or_else(|| CmdError::CommandNotFound)?)(writer, sub_args)
        }
    }
}

impl<'a, F> Commands<'a, F>
where
    F: FnMut(Rc<RefCell<dyn Write>>, &[String]) -> Result<(), CmdError>,
{
    pub fn run(
        &mut self,
        args: &[String],
        writer: Rc<RefCell<dyn Write>>,
        program_name: String,
    ) -> Result<(), CmdError> {
        let app_args = args.get(1..).ok_or(CmdError::MissingArguments)?;

        if app_args.is_empty() {
            self.help(writer, None, &program_name)?;
            return Ok(());
        }

        match (app_args[0].to_lowercase().as_str(), app_args.get(1..)) {
            ("help", Some(_)) => self.help(writer, None, &program_name)?,

            (cmd_name, Some(sub_args)) => {
                let found_command = self
                    .find_command_by_name(cmd_name)
                    .ok_or(CmdError::NoCommandHandlerFound)?;

                found_command.run(writer, sub_args)?;
            }
            _ => return Err(CmdError::UnknownCommand),
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use std::io;

    use crate::models::{CmdFn, Name};

    use super::*;

    struct TestWriter {
        buffer: Vec<u8>,
    }

    impl TestWriter {
        fn new() -> TestWriter {
            TestWriter { buffer: Vec::new() }
        }
    }

    impl Write for TestWriter {
        fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
            self.buffer.extend_from_slice(buf);
            Ok(buf.len())
        }

        fn flush(&mut self) -> io::Result<()> {
            Ok(())
        }
    }

    fn get_to_upper() -> CmdFn {
        Box::new(
            |writer: Rc<RefCell<dyn Write>>, args: &[String]| -> Result<(), CmdError> {
                let result: Vec<String> = args.into_iter().map(|arg| arg.to_uppercase()).collect();
                let mut writer = writer.borrow_mut();

                result
                    .iter()
                    .try_for_each(|item: &String| -> Result<(), CmdError> {
                        write!(writer, "{}", item)?;
                        Ok(())
                    })?;

                Ok(())
            },
        )
    }

    fn get_to_lower() -> CmdFn {
        Box::new(
            |writer: Rc<RefCell<dyn Write>>, args: &[String]| -> Result<(), CmdError> {
                let result: Vec<String> = args.into_iter().map(|arg| arg.to_lowercase()).collect();
                let mut writer = writer.borrow_mut();

                result
                    .iter()
                    .try_for_each(|item: &String| -> Result<(), CmdError> {
                        write!(writer, "{}", item)?;
                        Ok(())
                    })?;

                Ok(())
            },
        )
    }
    #[test]
    fn should_show_help() {
        //given
        let commands = Commands {
            command_list: vec![
                Command {
                    name: Name("upper command".to_string()),
                    description: "a command to make all text upper",
                    func: Some(get_to_lower()),
                    sub_commands: None,
                },
                Command {
                    name: Name("lower".to_string()),
                    description: "a command to make all text lower",
                    func: Some(get_to_upper()),
                    sub_commands: None,
                },
                Command {
                    name: Name("case".to_string()),
                    description:
                        "a command to change the case of a text which accepts sub commands",
                    func: None,
                    sub_commands: Some(Box::new(Commands {
                        command_list: vec![
                            Command {
                                name: Name("lower".to_string()),
                                description: "lowers the case of the arguments",
                                func: Some(get_to_lower()),
                                sub_commands: None,
                            },
                            Command {
                                name: Name("upper".to_string()),
                                description: "uppers the case of the arguments",
                                func: Some(get_to_upper()),
                                sub_commands: None,
                            },
                        ],
                        padding: 4,
                    })),
                },
            ],
            padding: 0,
        };

        let acc: Rc<RefCell<Vec<u8>>> = Rc::new(RefCell::new(Vec::new()));
        //when
        let res = commands.help(acc.clone(), None, "test");

        // //then
        assert_eq!(res.is_err(), false);
        assert_eq!(String::from_utf8(acc.clone().borrow().to_vec()).unwrap(),  "\nUsage:   test [COMMAND]\nexample: test help\n\nCOMMAND list:\n\n  upper command   - a command to make all text upper\n  lower   - a command to make all text lower\n  case   - a command to change the case of a text which accepts sub commands\n\n    Usage:   case [Sub command]\n    example: case help\n\n    Sub command list:\n\n      lower   - lowers the case of the arguments\n      upper   - uppers the case of the arguments\n\n")
    }

    #[test]
    fn should_run_sub_command_from_list_with_any_writer() {
        //given
        let commands_list: Vec<Command<CmdFn>> = vec![Command {
            name: Name("case".to_string()),
            description: "a command to change the case of a text which accepts sub commands",
            func: None,
            sub_commands: Some(Box::new(Commands {
                command_list: vec![
                    Command {
                        name: Name("lower".to_string()),
                        description: "lowers the case of the arguments",
                        func: Some(get_to_lower()),
                        sub_commands: None,
                    },
                    Command {
                        name: Name("upper".to_string()),
                        description: "uppers the case of the arguments",
                        func: Some(get_to_upper()),
                        sub_commands: None,
                    },
                ],
                padding: 4,
            })),
        }];

        let mut commands = Commands {
            command_list: commands_list,
            padding: 6,
        };

        let writer1: Rc<RefCell<Vec<u8>>> = Rc::new(RefCell::new(Vec::new()));
        let writer2: Rc<RefCell<TestWriter>> = Rc::new(RefCell::new(TestWriter::new()));

        let args_for_writer1 = [
            "test.program".to_string(),
            "case".to_string(),
            "lower".to_string(),
            "TEST".to_string(),
        ]
        .to_vec();

        let args_for_writer2 = [
            "test.program".to_string(),
            "case".to_string(),
            "upper".to_string(),
            "test".to_string(),
        ]
        .to_vec();

        //when
        let res_for_writer1 = commands.run(
            &args_for_writer1,
            writer1.clone(),
            "test.program".to_string(),
        );

        let res_for_writer2 = commands.run(
            &args_for_writer2,
            writer2.clone(),
            "test.program".to_string(),
        );

        //then
        assert!(res_for_writer1.is_ok());
        assert_eq!(
            String::from_utf8(writer1.clone().borrow().to_vec()).unwrap(),
            "test"
        );
        assert!(res_for_writer2.is_ok());
        assert_eq!(
            String::from_utf8(writer2.clone().borrow().buffer.clone()).unwrap(),
            "TEST"
        );
    }
}
