use thiserror::Error;


#[derive(Error, Debug)]
pub enum CmdError {
    #[error("Help was not found for input command")]
    HelpNotFound,
    #[error("Run command handler not found for this argument")]
    NoCommandHandlerFound,
    #[error("Command not found")]
    CommandNotFound,
    #[error("Subcommand not found")]
    SubcommandNotFound,
    #[error("Missing arguments to run command")]
    MissingArguments,
    #[error("Unknown app error")]
    UnknownCommand,
    #[error("Underlying IO error")]
    IoError(#[from] std::io::Error),
}

#[derive(Error, Debug, PartialEq)]
pub enum CommandBuilderError {
    #[error("a command needs a name")]
    NameNotFound,
    #[error("a command needs a description")]
    DescriptionNotFound,
}

#[derive(Error, Debug)]
pub enum CommandsBuilderError {
    #[error("a command list need to exist")]
    CommandsNotFound,
}