use std::{cell::RefCell, fmt::Display, io::Write, rc::Rc};

use crate::errors::CmdError;


pub type CmdFn = Box<dyn FnMut(Rc<RefCell<dyn Write>>, &[String]) -> Result<(), CmdError>>;

pub struct Command<'a, F> {
    pub name: Name,
    pub description: &'a str,
    pub func: Option<F>,
    pub sub_commands: Option<Box<Commands<'a, F>>>,
}


pub struct Commands<'a, F> {
    pub command_list: Vec<Command<'a, F>>,
    pub padding: usize,
}

// builders
pub struct CommandBuilder<'a, F, N> {
    pub name: N,
    pub description: Option<&'a str>,
    pub func: Option<F>,
    pub sub_commands: Option<Box<Commands<'a, F>>>,
}


pub struct CommandsBuilder<'a, F> {
    pub command_list: Option<Vec<Command<'a, F>>>,
    pub padding: Option<usize>,
}

// type states
#[derive(Debug)]
pub struct Name(pub String);

impl Display for Name {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)    
    }
}

impl PartialEq<str> for Name{
    fn eq(&self, other: &str) -> bool {
        self.0 == other
    }
}

pub struct NoName;
pub struct Description(pub String);
pub struct NoDescription;